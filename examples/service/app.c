#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>

#include "erpc.h"

void *business(void *arg)
{
    sleep(1);
    while(1)
    {
        erpc_service_proxy_call("hello", "helloworld", NULL, NULL, NULL);
        sleep(3);
    }
}

pthread_t business_id;

int main(void)
{
    erpc_framework_init("app");

    if(0 != pthread_create(&business_id, NULL, business, NULL))
        return -1;

    return erpc_framework_loop(ERPC_LOOP_DEFAULT);
}

