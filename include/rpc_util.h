/*
 * MIT License
 * 
 * Copyright (c) 2019 极简美 @ konishi5202@163.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __ERPC_UTIL_H__
#define __ERPC_UTIL_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "cjson/cJSON.h"

/**********************************************************
 * description: get erpc version
***********************************************************/
extern char *erpc_version(void);

/**********************************************************
 * description: get erpc running state
 * return : 0, erpc is running;
 *         -1, erpc is exit;
***********************************************************/
extern int erpc_is_running(void);

/**********************************************************
 * description: get sender name from sockfd
 * return : !NULL , sender name;
 *           NULL , get failed;
***********************************************************/
extern char *erpc_get_sender(int sockfd);

/**********************************************************
 * description: get current process name
 * return : !NULL , process name;
 *           NULL , get failed;
***********************************************************/
extern char *erpc_current_process_name(void);

/**********************************************************
 * description: duplicate cJSON object
 * parameter : params, to be duplicate object;
 *             object, the pointer point duplicate object;
 * return : 0 , duplicate OK, object point the new cJSON;
 *         -1 , duplicate failed;
***********************************************************/
extern int erpc_duplicate_params(cJSON *params, cJSON **object);

/**********************************************************
 * description: judge two string whether equal
 * parameter : str1, str2 to be compare string
 * return : 0 , equal
 *         -1 , not equal;
***********************************************************/
extern int erpc_string_name_equal(const char *str1, const char *str2);

/**********************************************************
 * description: change erpc timer period time
 * parameter : tv: tv_sec and tv_usec
***********************************************************/
extern void erpc_timer_period_set(struct timeval tv);

/**********************************************************
 * description: change erpc timer handler
 * parameter : handler, the pointer of function;
 *             arg, the params of timer handler.
***********************************************************/
extern void erpc_timer_handler_set(void (*handler)(void *arg), void *arg);

typedef void (*rpc_message_handler_t)(int sockfd, char *data, size_t len);
typedef int (*rpc_protocol_encrypt_t)(char *data, size_t len, int sockfd, rpc_message_handler_t handler);
typedef int (*rpc_protocol_decrypt_t)(char *data, size_t len, int sockfd, rpc_message_handler_t handler);
/**********************************************************
 * description: set information transmit security algorithm.
 * parameter : encrypt , encryption algorithm handler;
 *             decrypt , decryption algorithm handler;
***********************************************************/
extern void erpc_information_security(rpc_protocol_encrypt_t encrypt, rpc_protocol_decrypt_t decrypt);

#ifdef __cplusplus
}
#endif

#endif // __ERPC_UTIL_H__

