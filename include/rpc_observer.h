/*
 * MIT License
 * 
 * Copyright (c) 2019 极简美 @ konishi5202@163.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __RPC_OBSERVER_H__
#define __RPC_OBSERVER_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "cjson/cJSON.h"

typedef void (*erpc_observer_callback_t)(cJSON *params);

/**********************************************************
 * description: create a observed
 * parameter : module , the module name of observed belong to;
 *             observed, the name of observed;
 * return : 0 , create ok;
 *         -1 , create failed;
***********************************************************/
extern int erpc_observed_create(const char *module, const char *observed);

/**********************************************************
 * description: destroy a observed
 * parameter : module , the module name of observed belong to;
 *             observed, the name of observed;
 * return : 0 , destroy ok;
 *         -1 , destroy failed;
***********************************************************/
extern int erpc_observed_destroy(const char *module, const char *observed);

/**********************************************************
 * description: invoke/emit all observers
 * parameter : module , the module name of observed belong to;
 *             observed, the name of observed;
 *             params, the params of observed for invoke;
 * return : 0 , invoke/emit ok;
 *         -1 , invoke/emit failed;
***********************************************************/
extern int erpc_observer_invoke(const char *module, const char *observed, cJSON *params);

/**********************************************************
 * description: register a observer
 * parameter : module , the module name of observed belong to;
 *             observed, the name of observed;
 *             handler, the pointer point to observer handler;
 *             timeout, the max response time of remote observed;
 * return : 0 , register ok;
 *         -1 , register failed;
***********************************************************/
extern int erpc_observer_register(const char *module, const char *observed, erpc_observer_callback_t handler, struct timeval *timeout);

/**********************************************************
 * description: unregister a observer
 * parameter : module , the module name of observed belong to;
 *             observed, the name of observed;
 *             timeout, the max response time of remote observed;
 * return : 0 , unregister ok;
 *         -1 , unregister failed;
***********************************************************/
extern int erpc_observer_unregister(const char *module, const char *observed, struct timeval *timeout);

#ifdef __cplusplus
}
#endif

#endif  // __RPC_OBSERVER_H__


